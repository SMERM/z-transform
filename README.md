# *Z-transform* theory and coding

Theory and coding of the `z-transform`

The theory is explained [here](./theory/intro.md)

# AUTHOR

Nicola Bernardini <nic.bern.commercial@gmail.com>

# LICENSE

GNU GPL v.3.0
