# What is the `z-transform`

The `z-transform`, as the very name says, is an algorithm which belongs to the
huge family of mathematical transformations, that is operations that change
the domain of operation of a function. In our case, the transformation is:

```math
	X(z) = Z\{ x(n) \} = \sum_{n = -\infty}^{+\infty}{x(n) z^{-n}}~~~~~(1)
```

where:
* $x(n)$ is the actual (real or complex) function you want to transform
* $z^{-n}$ is a complex number (any complex number will do, although there are good reasons to pick specific values, as we will see later)
* $X(z)$ is the resulting `z-transform` of that function measured for the picked value of $z$

Since in practice we cannot deal with infinite numbers, we usually use a
limited version of the `z-transform`:

```math
	X(z) = \sum_{n = -N}^{+N-1}{x(n) z^{-n}}~~~~~(2) 
```

where:
* $N$ is the (discrete) number of samples $n$ that we decide to pick

or even the *one-sided* `z-transform`

```math
	X(z) = \sum_{n = 0}^{+N-1}{x(n) z^{-n}}~~~~~(3)
```

which is the widest and most commonly used operation.

Let us unroll the summation operator on (3) to understand what is going on:

```math
	X(x) = x(0) z^{-0} + x(1) z^{-1} + x(2) z^{-2} + ... + x(N-1) z^{-N-1}~~(4)
```

Equation (4) clarifies the usefulness of the `z-transform`:

1. it orders the values of a discrete signal. A discrete signal is a sequence
   of values which are in a certain order, but this order is not coded
   *inside* each value. In short, you could shuffle the order of samples and a
   sum of all samples would give exactly the same result because addition is
   commutative. Not so after `z-transforming` the signal.
2. more importantly, the `z-transform` changes the signal into a polynomial
   (of degree $N$ in our last equation (4)). Polynomials are a very
   well-studied field in mathematics (the field of *linear algebra*) and,
   although not all of them have solutions, many tools have been developed to
   solve them with at least some degree of approximation.
3. furthermore, transforming signals into polynomials allows us to understand
   many useful operations that we can perform on them (more about this later)

This works, of course, only on discrete-time systems. In fact, the
`z-transform` is the discrete form of another transform, the `Laplace
transform`, which works for continuous functions.

<!--
# Examples of `z-transform` (with code)

* a signal (noise, ordered 1ones etc.)

* z-transform on a z-plane

* z-transform on a unit circle

# Properties of the `z-transform`

-->
